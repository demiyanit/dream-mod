
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.client.gui.screens.MenuScreens;

import net.mcreator.axegaocraftatthedreams.client.gui.Dreamcraftinggui1Screen;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class AxegaocraftatthedreamsModScreens {
	@SubscribeEvent
	public static void clientLoad(FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			MenuScreens.register(AxegaocraftatthedreamsModMenus.DREAMCRAFTINGGUI_1.get(), Dreamcraftinggui1Screen::new);
		});
	}
}
