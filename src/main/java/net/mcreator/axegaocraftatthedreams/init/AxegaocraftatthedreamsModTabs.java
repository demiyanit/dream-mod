
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class AxegaocraftatthedreamsModTabs {
	public static CreativeModeTab TAB_DREAMS_TAB;

	public static void load() {
		TAB_DREAMS_TAB = new CreativeModeTab("tabdreams_tab") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(Items.MAP);
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
