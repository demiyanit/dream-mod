
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.biome.Biome;

import net.mcreator.axegaocraftatthedreams.world.biome.DreamBiomeBiome;
import net.mcreator.axegaocraftatthedreams.AxegaocraftatthedreamsMod;

public class AxegaocraftatthedreamsModBiomes {
	public static final DeferredRegister<Biome> REGISTRY = DeferredRegister.create(ForgeRegistries.BIOMES, AxegaocraftatthedreamsMod.MODID);
	public static final RegistryObject<Biome> DREAM_BIOME = REGISTRY.register("dream_biome", () -> DreamBiomeBiome.createBiome());
}
