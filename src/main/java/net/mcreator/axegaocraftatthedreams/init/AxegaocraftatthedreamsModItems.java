
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.axegaocraftatthedreams.item.DreamuniverseItem;
import net.mcreator.axegaocraftatthedreams.AxegaocraftatthedreamsMod;

public class AxegaocraftatthedreamsModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, AxegaocraftatthedreamsMod.MODID);
	public static final RegistryObject<Item> DREAM_BLOCK = block(AxegaocraftatthedreamsModBlocks.DREAM_BLOCK,
			AxegaocraftatthedreamsModTabs.TAB_DREAMS_TAB);
	public static final RegistryObject<Item> DREAM_BLOCK_DUN = block(AxegaocraftatthedreamsModBlocks.DREAM_BLOCK_DUN,
			AxegaocraftatthedreamsModTabs.TAB_DREAMS_TAB);
	public static final RegistryObject<Item> DREAMUNIVERSE = REGISTRY.register("dreamuniverse", () -> new DreamuniverseItem());
	public static final RegistryObject<Item> DREAMCRAFTINGTABLE = block(AxegaocraftatthedreamsModBlocks.DREAMCRAFTINGTABLE,
			AxegaocraftatthedreamsModTabs.TAB_DREAMS_TAB);

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
