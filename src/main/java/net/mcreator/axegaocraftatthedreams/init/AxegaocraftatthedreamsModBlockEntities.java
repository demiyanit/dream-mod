
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.mcreator.axegaocraftatthedreams.block.entity.DreamcraftingtableBlockEntity;
import net.mcreator.axegaocraftatthedreams.AxegaocraftatthedreamsMod;

public class AxegaocraftatthedreamsModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES,
			AxegaocraftatthedreamsMod.MODID);
	public static final RegistryObject<BlockEntityType<?>> DREAMCRAFTINGTABLE = register("dreamcraftingtable",
			AxegaocraftatthedreamsModBlocks.DREAMCRAFTINGTABLE, DreamcraftingtableBlockEntity::new);

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block,
			BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
