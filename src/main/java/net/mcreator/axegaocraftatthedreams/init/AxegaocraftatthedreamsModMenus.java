
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.extensions.IForgeMenuType;

import net.minecraft.world.inventory.MenuType;

import net.mcreator.axegaocraftatthedreams.world.inventory.Dreamcraftinggui1Menu;
import net.mcreator.axegaocraftatthedreams.AxegaocraftatthedreamsMod;

public class AxegaocraftatthedreamsModMenus {
	public static final DeferredRegister<MenuType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.MENU_TYPES, AxegaocraftatthedreamsMod.MODID);
	public static final RegistryObject<MenuType<Dreamcraftinggui1Menu>> DREAMCRAFTINGGUI_1 = REGISTRY.register("dreamcraftinggui_1",
			() -> IForgeMenuType.create(Dreamcraftinggui1Menu::new));
}
