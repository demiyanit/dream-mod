
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.axegaocraftatthedreams.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.mcreator.axegaocraftatthedreams.block.DreamuniversePortalBlock;
import net.mcreator.axegaocraftatthedreams.block.DreamcraftingtableBlock;
import net.mcreator.axegaocraftatthedreams.block.DreamBlockDunBlock;
import net.mcreator.axegaocraftatthedreams.block.DreamBlockBlock;
import net.mcreator.axegaocraftatthedreams.AxegaocraftatthedreamsMod;

public class AxegaocraftatthedreamsModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, AxegaocraftatthedreamsMod.MODID);
	public static final RegistryObject<Block> DREAM_BLOCK = REGISTRY.register("dream_block", () -> new DreamBlockBlock());
	public static final RegistryObject<Block> DREAM_BLOCK_DUN = REGISTRY.register("dream_block_dun", () -> new DreamBlockDunBlock());
	public static final RegistryObject<Block> DREAMUNIVERSE_PORTAL = REGISTRY.register("dreamuniverse_portal", () -> new DreamuniversePortalBlock());
	public static final RegistryObject<Block> DREAMCRAFTINGTABLE = REGISTRY.register("dreamcraftingtable", () -> new DreamcraftingtableBlock());
}
